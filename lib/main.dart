import 'package:cpsp_app/src/app.dart';
import 'package:cpsp_app/src/utils/my_resources.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      //statusBarColor: Colors.transparent,
      //systemNavigationBarColor: MyColors.colorWhite,
      //systemNavigationBarIconBrightness: Brightness.dark,
    ),
  );
  runApp(App());
}
