import 'package:cpsp_app/src/ui/widgets/widgets.dart';
import 'package:cpsp_app/src/utils/my_resources.dart';

import 'package:flutter/material.dart';


class CoursePage extends StatelessWidget {

  final List<Widget> items = [
    CardCourse(
      title: 'Curso de manejo clínico terapeútico en tiempos de COVID',
      color: Colors.blue[700]!,
      videos: 27,
      hours: 60,
    ),
    CardCourse(
      title: 'Acción psicólogica en tiempos de pandemia',
      color: Colors.red[700]!,
      videos: 9,
      hours: 20,
    ),
    CardCourse(
      title: 'Impacto psicológico del COVID en los equipos',
      color: Colors.pink[700]!,
      videos: 7,
      hours: 15,
    ),
    CardCourse(
      title: 'Curso de manejo clinico terapeútico en tiempos de COVID',
      color: Colors.purple[700]!,
      videos: 36,
      hours: 60,
    ),
    CardCourse(
      title: 'Curso de manejo clinico terapeutico en tiempos de COVID',
      color: Colors.green[700]!,
      videos: 36,
      hours: 60,
    ),
    CardCourse(
      title: 'Curso de manejo clinico terapeutico en tiempos de COVID',
      color: Colors.orange[700]!,
      videos: 36,
      hours: 60,
    ),
    CardCourse(
      title: 'Curso de manejo clinico terapeutico en tiempos de COVID',
      color: Colors.blue[700]!,
      videos: 36,
      hours: 60,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          SafeArea(
            child: Padding(
              padding: const EdgeInsets.only( top: 16, left: 10 ),
              child: _buildTitle(),
            ),
          ),

          ...items

        ],
      ),
    );
  }

  Widget _buildTitle() {
    return Stack(
      children: [
         
        Positioned(
          bottom: 8,
          child: Container(
            width: 150,
            height: 4,
            decoration: const BoxDecoration(
              color: MyColors.secondaryColorLight,
            ),
          ),
        ),
        const Text(
          'Cursos',
          style: TextStyle(
            fontSize: 32,
          ),
        ),

      ],
    );
  }
}
