import 'package:cpsp_app/src/ui/pages/course_page.dart';
import 'package:cpsp_app/src/ui/pages/home_page.dart';
import 'package:cpsp_app/src/ui/pages/user_page.dart';

import 'package:flutter/material.dart';

class CpspApp extends StatefulWidget {

  @override
  _CpspAppState createState() => _CpspAppState();
}

class _CpspAppState extends State<CpspApp> {

  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    CoursePage(),
    UserPage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: _children[_currentIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onChangeHome,
        currentIndex: _currentIndex,
        items: const [

          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bookmark),
            label: 'Cursos',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Usuario',
          ),

        ],
      ),
    );
  }

  void onChangeHome(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

}
