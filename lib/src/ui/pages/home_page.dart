import 'package:cpsp_app/src/ui/widgets/widgets.dart';

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
    
          _MainScroll(),
    
        ],
      ),
    );
  }
}

class _MainScroll extends StatelessWidget {
  
  final itemsCardsNews = const [
    CardNews(title: 'El Colegio de Psicologos del Peru convoca a los miembros del comite',),
    CardNews(title: 'Viciministro somos una orden no un gremio',),
    CardNews(title: 'Fallecimiento del psicólogo Luis Oblitas',),
    CardNews(title: 'Se acerca el segundo congreso',),
    CardNews(title: 'Se acerca el segundo congreso',),
    CardNews(title: 'Se acerca el segundo congreso',)
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: [

          SliverPersistentHeader(
            floating: true,
            delegate: _SliverCustomHeaderDelegate(
              minHeight: 200,
              maxHeight: 400,
              child: Container(
                alignment: Alignment.centerLeft,
                color: Colors.white,
                child: HeaderPico(),
              ),
            ),
          ),

          SliverList(
            delegate: SliverChildListDelegate([

              Container(
                alignment: Alignment.centerLeft,
                height: 70,
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                margin: const EdgeInsets.symmetric(vertical: 5),
                child: const Text('Ultimas Noticias', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500)),
              ),

              ...itemsCardsNews,
              const SizedBox( height: 50 )
            ]),
          ),

        ],  
      ),
    );
  }
}

class _SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {

  final double minHeight;
  final double maxHeight;
  final Widget child;

  _SliverCustomHeaderDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand( child: child );
  }

  @override
  double get maxExtent => ( minHeight > maxHeight ) ? minHeight : maxHeight;

  @override
  double get minExtent => ( minHeight < maxHeight ) ? minHeight : maxHeight;

  @override
  bool shouldRebuild(covariant _SliverCustomHeaderDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
           minHeight != oldDelegate.minHeight ||
           child     != oldDelegate.child;
  }

}
