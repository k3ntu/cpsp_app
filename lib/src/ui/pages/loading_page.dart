import 'package:cpsp_app/src/helpers/helpers.dart';
import 'package:cpsp_app/src/ui/pages/acceso_gps_page.dart';
import 'package:cpsp_app/src/ui/pages/mapa_page.dart';

import 'package:flutter/material.dart';

import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';



class LoadingPage extends StatefulWidget {

  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> with WidgetsBindingObserver {

  @override
  void initState() {

    WidgetsBinding.instance!.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {

    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  // Para saber el estado de la aplicacion
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {

    if (state == AppLifecycleState.resumed) {
      if ( await Geolocator.isLocationServiceEnabled() ) {
        Navigator.pushReplacement(context, navigateFadeIn( MapaPage() ));
      }
    }

    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkGpsYLocation(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {

          if (snapshot.hasData) {
            return Center(child: Text( snapshot.data.toString() ));
          } else {
            return const Center(child: CircularProgressIndicator(strokeWidth: 2 ) );
          }
          
        },
      ),
   );
  }

  Future checkGpsYLocation( BuildContext context ) async {
    final navigator = Navigator.of(context);
    // Permiso GPS
    final permisoGPS = await Permission.location.isGranted;
    // GPS está activo
    final gpsActivo = await Geolocator.isLocationServiceEnabled();

    if (permisoGPS && gpsActivo) {
      navigator.pushReplacement( navigateFadeIn( MapaPage() ));
      return '';
    } else if ( !permisoGPS ) {
      navigator.pushReplacement( navigateFadeIn( AccesoGpsPage() ));
      return 'Es necesario el Permiso de GPS';
    } else if ( !gpsActivo ) {
      return 'Active el GPS';
    }

    print('Loading Page Hola Mundo!!');

    // Navigator.pushReplacement(context, navegarMapaFadeIn(context, AccesoGpsPage() ));
    // Navigator.pushReplacement(context, navegarMapaFadeIn(context, MapaPage() ));
  }

}
