import 'package:cpsp_app/src/domain/bloc/mapa/mapa_bloc.dart';
import 'package:cpsp_app/src/domain/bloc/mi_ubicacion/mi_ubicacion_bloc.dart';
import 'package:cpsp_app/src/ui/widgets/widgets.dart';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapaPage extends StatefulWidget {
  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {

  @override
  void initState() {
    BlocProvider.of<MiUbicacionBloc>(context).iniciarSeguimiento();

    super.initState();
  }

  @override
  void dispose() {
    BlocProvider.of<MiUbicacionBloc>(context).cancelarSeguimiento();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          BlocBuilder<MiUbicacionBloc, MiUbicacionState>(
            builder: ( _ , state) => crearMapa( state ),
          ),

          // TODO: hacel el tootgle cuando estoy manualmente
          Positioned(
            top: 15,
            child: SelectRegion(),
          ),

        ],
      ),

      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [

          BtnUbicacion(),

        ],
      ),
    );
  }


  Widget crearMapa(MiUbicacionState state) {

    if ( !state.existeUbicacion ) return const Center(child: Text('Ubicando...'));

    final mapaBloc = BlocProvider.of<MapaBloc>(context);

    mapaBloc.add( OnLocationUpdate( state.ubicacion! ) );

    final cameraPosition = CameraPosition(
      target: state.ubicacion!,
      zoom: 15.0,
    );

    return GoogleMap(
      initialCameraPosition: cameraPosition,
      // myLocationEnable: punto que muestra mi ubicación actual
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      zoomControlsEnabled: false,
      //onMapCreated: ( GoogleMapController controller ) => mapaBloc.initMapa(controller)
      onMapCreated: mapaBloc.initMapa,
      polylines: mapaBloc.state.polylines.values.toSet(),
      onCameraMove: ( cameraPosition ) {
        // cameraPosition.target = LatLng central del mapa
        mapaBloc.add( OnMovioMapa( cameraPosition.target ) );
      },
      // Se ejecuta cada vez que finaliza el moviento de la camara
      // onCameraIdle: () {},
    );

  }

}
