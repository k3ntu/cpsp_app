import 'package:cpsp_app/src/ui/widgets/widgets.dart';
import 'package:cpsp_app/src/utils/my_resources.dart';

import 'package:flutter/material.dart';


class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            HeaderOlas(),

            SafeArea(
              child: ListView(
                keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                children: [

                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height - (AppBar().preferredSize.height * 0.60),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [

                        const Logo(title: 'C.Ps.P App'),
                        Flexible(
                          flex: 6,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: MySizes.pxScreen),
                            child: _buildLogin(),
                          ),
                        ),

                      ],
                    ),
                  ),
                  

                ],
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget _buildLogin() {
    return Column(
      children: [

        Flexible(
          child: Column(
            children: [

              BtnNormal(
                'Actualizar datos'
              ),

            ]
          )
        ),

      ],
    );
  }
}
