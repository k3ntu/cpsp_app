part of 'widgets.dart';

class Logo extends StatelessWidget {

  final String title;

  const Logo({
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 170,
        margin: const EdgeInsets.only(top: 30),
        child: Column(
          children: [

            const Image( image: AssetImage('assets/images/cpsp-logo.png') ),
            const SizedBox(height: 20),
            Text(title, style: const TextStyle(fontSize: 30))

          ],
        ),
      ),
    );
  }

}
