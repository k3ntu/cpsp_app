part of 'widgets.dart';

class BtnNormal extends StatelessWidget {

  const BtnNormal(
    this.name,
    {
      this.actionButton,
      this.opacity = 1.0,
      this.canRevertedColor = false,
      this.colorPrimary = MyColors.primaryColor,
      this.colorSecondary = MyColors.colorWhite,
      this.radius = 1000,
      this.padding,
      this.icon,
      this.suffixIcon,
      this.fontWeight = FontWeight.w600,
      this.hasGrid = true,
    }
  );

  final String name;
  final void Function()? actionButton;
  final double opacity;
  final bool canRevertedColor;
  final Color colorPrimary;
  final Color colorSecondary;
  final double radius;
  final EdgeInsets? padding;
  final Widget? icon;
  final Widget? suffixIcon;
  final FontWeight fontWeight;
  final bool hasGrid;

  @override
  Widget build(BuildContext context) {
    final sizedWidget = MediaQuery.of(context).size.width;

    Color buttonColor;
    Color fontColor;

    if (canRevertedColor) {
      buttonColor = colorSecondary;
      fontColor = colorPrimary;
    } else {
      buttonColor = colorPrimary;
      fontColor = Colors.white;
    }

    return InkWell(
      onTap: actionButton,
      child: Opacity(
        opacity: opacity,
        child: Container(
          width: double.infinity,
          height: ( padding != null ) ? null : sizedWidget / 7.2,
          padding: padding,
          decoration: BoxDecoration(
            color: actionButton != null ? buttonColor : MyColors.colorGrey,
            borderRadius: BorderRadius.circular( radius ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              _buildItem(
                weight: 2,
                alignment: Alignment.centerRight,
                child: icon ?? Container(),
              ),
              _buildItem(
                weight: 5,
                child: Text(
                  name,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: fontColor,
                    fontWeight: fontWeight,
                    fontSize: MySizes.sizeSubtitle,
                  ),
                ),
              ),
              _buildItem(
                weight: 2,
                alignment: Alignment.centerLeft,
                child: suffixIcon ?? Container(),
              )


            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItem({ required int weight, required Widget child, Alignment? alignment }) {
    Widget? item;

    if (hasGrid) {
      item = Flexible(
        flex: weight,
        child: Container(
          alignment: alignment,
          width: double.infinity,
          child: child,
        ),
      );
    }

    return item ?? child;
  }

}
