part of 'widgets.dart';

class HeaderPico extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [

        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _HeaderPicoPainter(),
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 90, right: 36, left: 36),
          child: buildCategoryMenu(),
        ),
        
      ],
    );
  }

  Widget buildCategoryMenu() {
    return SizedBox(
        child: Wrap(
          runSpacing: 20,
          children: [

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Hero(
                  tag: "category_advert",
                  child: GestureDetector(
                    onTap: () => {
                    },
                    child: const CardCategoryMenu(
                      'Anuncios',
                      icon: Icon(Icons.new_releases_outlined, color: MyColors.secondaryColorDark ),
                    ),
                  ),
                ),
                Hero(
                  tag: "category_formalities",
                  child: GestureDetector(
                    onTap: () => {
                    },
                    child: const CardCategoryMenu(
                      'Trámites',
                      icon: Icon(Icons.dashboard_customize, color: MyColors.secondaryColorDark ),
                    ),
                  ),
                ),
              ],
            ),

            const SizedBox(
              height: 10,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Hero(
                  tag: "category_library",
                  child: GestureDetector(
                    onTap: () => {
                    },
                    child: const CardCategoryMenu(
                      'Bliblioteca',
                      icon: Icon(Icons.library_books, color: MyColors.secondaryColorDark ),
                    ),
                  ),
                ),
                Hero(
                  tag: "category_thesis",
                  child: GestureDetector(
                    onTap: () => {
                    },
                    child: const CardCategoryMenu(
                      'Tesis',
                     icon: Icon(Icons.bookmarks_sharp, color: MyColors.secondaryColorDark ),
                    ),
                  ),
                ),
              ],
            ),

          ],
        ),
      );
  }
}

class _HeaderPicoPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = MyColors.primaryColor;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 2;

    final path = Path();

    path.lineTo(0, size.height * 0.80);
    path.lineTo(size.width * 0.5, size.height * 0.95);
    path.lineTo(size.width, size.height * 0.80);
    path.lineTo(size.width, 0);

    canvas.drawShadow(path.shift( const Offset(0, 2) ), Colors.black, 2.2, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

 // Class Waves
class HeaderOlas extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: CustomPaint(
        painter: _HeaderOlasPainter(),
      ),
    );
  }
}

class _HeaderOlasPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = const Color(0xff615AAB);
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 2;

    final path = Path();

    path.moveTo(0, size.height);
    path.lineTo(0, size.height * 0.75);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.85, size.width * 0.5, size.height * 0.75);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.65, size.width, size.height * 0.75);
    path.lineTo(size.width, size.height);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
  
}
