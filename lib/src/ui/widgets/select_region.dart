part of 'widgets.dart';

class SelectRegion extends StatefulWidget {

  @override
  _SelectRegionState createState() => _SelectRegionState();
}

class _SelectRegionState extends State<SelectRegion> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric( horizontal: 30 ),
        width: width,
        child: GestureDetector(
          onTap: () async {
            print('Buscando....');

            final resultado = await showSearch(context: context, delegate: SearchDestination());
            retornoBusqueda( resultado! );

          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 13),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black12, blurRadius: 5, offset: Offset(0, 5),
                )
              ],
            ),
            child: const Text(
              '¿Encuentra tu CDR o Centro de vacunación?',
              style: TextStyle( color: Colors.black87 ),
            ),
          ),
        ),
      ),
    );
  
  }

  void retornoBusqueda( SearchResult result ) {

    print( 'cancelo: ${result.cancelo}' );
    print( 'manual: ${result.manual}' );

    if ( result.cancelo ) return;
    final mapaBloc = BlocProvider.of<MapaBloc>(context);
    
    mapaBloc.add(OnLocationUpdate( result.position! ));
    mapaBloc.moverCamara( result.position! );
  }

}
