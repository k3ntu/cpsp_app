part of 'widgets.dart';

class CardNews extends StatelessWidget {

  final String title;
 
  const CardNews({
    required this.title,
  });

  @override
  Widget build(BuildContext context) {

    return Container(
      alignment: Alignment.centerLeft,
      height: 90,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Colors.black45,
            spreadRadius: 0.1,
            blurRadius: 0.9,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: Row(
        children: [

          const CircleAvatar(
            child: Icon( Icons.new_releases_outlined ),
          ),

          const SizedBox(
            width: 8,
          ),

          _buildTitle(),

        ],
      ),
    );
  }

  Widget _buildTitle() {
    return Expanded(
      child: Text(
        title,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

}
