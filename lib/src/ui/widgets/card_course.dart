part of 'widgets.dart';

class CardCourse extends StatelessWidget {

  final String title;
  final Color color;
  final int videos;
  final int hours;

  const CardCourse({
    required this.title,
    required this.color,
    required this.videos,
    required this.hours,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 100,
      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 15),
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: color,
        boxShadow: const [
          BoxShadow(
            color: MyColors.colorGrey,
            spreadRadius: 0.1,
            blurRadius: 0.9,
            offset: Offset(0, 2),
          )
        ],
      ),
      child: _buildCard(),
    );
  }

  Widget _buildCard() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [

        const CircleAvatar(
          backgroundColor: Colors.transparent,
          child: Icon(Icons.bookmarks_sharp, color: Colors.white ),
        ),

        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [

              Text(
                title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const Icon(Icons.video_collection_rounded, color: Colors.white,),
                  Text(
                    '$videos',
                    style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.white),
                  ),

                  const SizedBox(width: 20),

                  const Icon(Icons.timer_sharp, color: Colors.white,),
                  Text(
                    '$hours',
                    style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.white),
                  ),
                ],
              )


            ],  
          ),
        ),
        
      ],
    );
  }

}
