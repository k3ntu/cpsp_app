part of 'widgets.dart';

class CardCategoryMenu extends StatelessWidget {

  final String title;
  final Icon icon;

  const CardCategoryMenu(
    this.title,
    {
      required this.icon,
    }
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      padding: const EdgeInsets.all(7),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          CircleAvatar(
            backgroundColor: Colors.transparent,
            child: icon,
          ),

          Text(
            title,
            style: const TextStyle(
              fontSize: 15,
            ),
          )

        ],
      ),
    );
  }

}
