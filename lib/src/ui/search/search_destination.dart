import 'package:cpsp_app/src/domain/models/search_result.dart';

import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class SearchDestination extends SearchDelegate<SearchResult> {

  @override
  final String searchFieldLabel;

  SearchDestination(): searchFieldLabel = 'Buscar...';

  @override
  List<Widget> buildActions(BuildContext context) {
    
    return [
      IconButton(
        icon: const Icon( Icons.clear ),
        onPressed: () => query = '',
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon( Icons.arrow_back ),
      onPressed: () => close(context, SearchResult(cancelo: true)),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return const Text('Build Results');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return ListView(
      children: [

        ListTile(
          leading: const Icon( Icons.location_on ),
          title: const Text('Colocar ubicación manualmente'),
          onTap: () {
            close(context, SearchResult(cancelo: false, manual: true));
          },
        ),
        ListTile(
          leading: const Icon( Icons.location_on ),
          title: const Text('CDR1 - Lima y Callao'),
          onTap: () {

            close(
              context, 
              SearchResult(
                cancelo: false,
                manual: false,
                nombreDestino: 'CDR1',
                position: const LatLng(-12.0658474, -77.0751486),
              ),
            );
          },
        )

      ],
    );
  }

}
