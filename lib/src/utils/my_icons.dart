
part of 'my_resources.dart';

// ignore_for_file: constant_identifier_names
class MyIcons {

  const MyIcons._();

  // App
  static const String LOGO_APP                     = 'assets/svgs/logo_with_host.svg';

  // Bottom Navigation Bar
  static const String HOME_CUSTOM                  = 'assets/svgs/home_custom.svg';
  static const String HAMBUERGUER_MENU_CUSTOM      = 'assets/svgs/hamburger_menu_custom.svg';

  static const String EMPTY_SCREEN_FRAGMENT        = 'assets/svgs/screen/screen_empty_fragment.svg';
  
  // Icons
  static const String IC_EMAIL                     = 'assets/svgs/email_custom.svg';
  static const String IC_PASSWORD                  = 'assets/svgs/password_custom.svg';
  static const String IC_PENCIL_EDIT               = 'assets/svgs/ic/ic_pencil_edit.svg';
  static const String IC_GRID_VIEW                 = 'assets/svgs/ic/ic_grid_view.svg';
  static const String IC_STATISTICS                = 'assets/svgs/ic/ic_statistics.svg';
  static const String IC_PERSON                    = 'assets/svgs/ic/ic_person.svg';

  static const String IC_LESS_NO_SELECTED          = 'assets/svgs/ic/ic_less_no_selected.svg';
  static const String IC_MORE_NO_SELECTED          = 'assets/svgs/ic/ic_more_no_selected.svg';

  static const String IC_BOX_NAME_CATEGORY         = 'assets/svgs/ic/ic_box_name_category.svg';
  static const String IC_BOX_NAME_ELEMENT          = 'assets/svgs/ic/ic_box_name_element.svg';
  static const String IC_BOX_DESCRIPTION           = 'assets/svgs/ic/ic_box_description.svg';
  static const String IC_BOX_PRICE                 = 'assets/svgs/ic/ic_box_price.svg';
  static const String IC_BOX_RANGE_HOUR            = 'assets/svgs/ic/ic_box_range_hour.svg';
  static const String IC_BOX_RANGE                 = 'assets/svgs/ic/ic_box_range.svg';
  static const String IC_BOX_QUANTITY_ORDERS       = 'assets/svgs/ic/ic_box_quantity_orders.svg';

  static const String IC_ACCESS_ALARMS             = 'assets/svgs/ic/ic_access_alarms.svg';
  static const String IC_PHOTO                     = 'assets/svgs/ic/ic_photo.svg';
  static const String IC_TYPE_FOOD                 = 'assets/svgs/ic/ic_type_food.svg';
  static const String IC_SERVICES                  = 'assets/svgs/ic/ic_services.svg';
  static const String IC_RANGE_DELIVERY            = 'assets/svgs/ic/ic_range_delivery.svg';
  static const String IC_PRICE_X_SERVICE           = 'assets/svgs/ic/ic_price_x_service.svg';
  static const String IC_ADDRESS_GPS               = 'assets/svgs/ic/ic_address_gps.svg';
  static const String IC_METHOD_PAY                = 'assets/svgs/ic/ic_method_pay.svg';
  static const String IC_CONTACT                   = 'assets/svgs/ic/ic_contact.svg';
  static const String IC_TIME                      = 'assets/svgs/ic/ic_time.svg';
  static const String IC_RUC                       = 'assets/svgs/ic/ic_ruc.svg';
  static const String EAT_IN_HERE                  = 'assets/svgs/eat_in_here.svg';
  static const String EAT_CARRY_OUT                = 'assets/svgs/eat_carry_out.svg';
  static const String EAT_AT_ADDRESS               = 'assets/svgs/eat_at_address.svg';

  static const String IC_CATEGORY_MENU             = 'assets/svgs/category/ic_menu.svg';
  static const String IC_CATEGORY_CRIOLLO          = 'assets/svgs/category/ic_criollo.svg';
  static const String IC_HAMBURGUER                = 'assets/svgs/category/ic_hamburguer.svg';

  static const String IC_CLEAN_HAND                = 'assets/svgs/ic/ic_clean_hand.svg';


  // Credits cards and methods of pay
  static const String CC_MASTERCARD                = 'assets/svgs/cc/cc_mastercard.svg';
  static const String CC_YAPE                      = 'assets/svgs/cc/cc_yape.png';

  // Countries
  static const String CON_PE                       = 'assets/svgs/contries/pe.svg';

  // Brands
  static const String FACEBOOK                     = 'assets/svgs/brands/facebook.svg';
  static const String INSTAGRAM                    = 'assets/svgs/brands/instagram.svg';
  static const String WHATSAPP                     = 'assets/svgs/brands/whatsapp.svg';
  static const String YOUTUBE                      = 'assets/svgs/brands/youtube.svg';


}
