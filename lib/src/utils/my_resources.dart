import 'dart:ui';

import 'package:sizer/sizer.dart';

part 'my_colors.dart';
part 'my_icons.dart';
part 'my_sizes.dart';
