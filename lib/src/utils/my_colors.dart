part of 'my_resources.dart';

class MyColors {

  MyColors._();

  // Colores primarys
  static const Color primaryColor = Color(0xFF022E8C);
  static const Color primaryColorDark = Color(0xFF02184F);
  static const Color primaryColorDark2 = Color(0xFF052978);
  static const Color primaryColorLight = Color(0xFF3967C9);
  static const Color primaryColorLight2 = Color(0xFFE4ECFF);

  // Colores secondarys
  static const Color secondaryColor = Color(0xFFFACB0F);
  static const Color secondaryColorDark = Color(0xFFCF8600);
  static const Color secondaryColorDark2 = Color(0xFFE99F00);
  static const Color secondaryColorLight = Color(0xFFF5D140);
  static const Color secondaryColorLight2 = Color(0xFFF7EAB5);

  // Colors active
  static const Color tertiaryColor = Color(0xFF2BD65E);

  // Colores de acentuacion
  static const Color accentColor = Color(0xFFFA003F);

  // Colors de texts
  static const Color primaryColorText = Color(0xFF2D2C29);
  static const Color secondaryColorText = Color(0xFFBCBAB5);

  // Escala de grises y blancos
  static const Color colorGrey = Color(0xFF85837F);
  static const Color colorGreyDark = Color(0xFF4D4C4A);
  static const Color colorWhite = Color(0xFFFFFCF5);
  static const Color colorBorder = Color(0xFFE8E8E8);

  // Color App
  static const Color colorBackgroundApp = Color(0xFFF7F6F5);

   // Colors actives
  static const Color colorSuccess     = Color(0xFF24B563);
  static const Color colorDanger      = Color(0xFFF60814);
  static const Color colorWarning     = Color(0xFFF2C80F);

}
