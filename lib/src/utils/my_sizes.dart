part of 'my_resources.dart';
/// Information about a piece of sizes standerd the App Restoner
///
/// For example, the [MySizes.sizeTitle] property contains the size percent (width screen / const)
class MySizes {

  // Fonts sizes
  static final double sizeHeadline   = 29.16.sp;
  static final double sizeHeader     = 23.3.sp;
  static final double sizeTitle      = 16.6.sp;
  static final double sizeSubtitle   = 12.5.sp;
  static final double sizeBody1      = 11.0.sp;
  static final double sizeBody2      = 9.0.sp;

  // Icons Sizes
  static final double icTiny         = 16.6.sp;
  static final double icNormal       = 19.0.sp;
  static final double icMedium       = 22.2.sp;
  static final double icLarge        = 30.5.sp;


  // Width columns
  // Equals to 40
  static final double w1Column  = 10.0.w;

  // Margins
  static final double mb1Card   = 1.25.h;
  static final double my1Card   = 3.0.h;
  static final double hr1       = 2.0.h;

  static final double py1Card   = 2.0.h;


  // Spaces sizes
  static final double pxScreen = 4.0.w;
  static final double pyScreen = 16.0.h;

  // Login
  static final double pxLoginLogo               = 10.0.w;
  static final double pxLoginTextFieldInCard    = 5.7.w;
  static final double pyLoginCardForm           = 4.5.h;
  static final double pyLoginButtonAndForm      = 9.0.h;
  static final double pyLoginButtonAndTerm      = 5.0.h;

  // Widths
  static final double wFragment4Photo           = 70.0.w;
  static final double hFragment4Photo           = 20.5.h;

  // Cards
  static final double wCardCategory     = 25.5.w;
  static final double rCard             = 3.75.w;

}
