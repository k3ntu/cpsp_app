import 'package:cpsp_app/src/ui/pages/activate_user_page.dart';
import 'package:cpsp_app/src/ui/pages/home_page.dart';
import 'package:cpsp_app/src/ui/pages/loading_page.dart';
import 'package:cpsp_app/src/ui/pages/login_page.dart';
import 'package:cpsp_app/src/ui/pages/mapa_page.dart';

import 'package:flutter/material.dart';
class RouterGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/loading':
        return MaterialPageRoute(builder: (_) => LoadingPage());
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/active':
        return MaterialPageRoute(builder: (_) => ActivarUserPage());
      case '/home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/mapa':
        return MaterialPageRoute(builder: (_) => MapaPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: const Center(
            child: Text('Error'),
          ),
        );
      },
    );
  }

}
