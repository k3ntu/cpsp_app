import 'package:cpsp_app/src/domain/bloc/mapa/mapa_bloc.dart';
import 'package:cpsp_app/src/domain/bloc/mi_ubicacion/mi_ubicacion_bloc.dart';
import 'package:cpsp_app/src/routers/routes.dart';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(create: ( _ ) => MiUbicacionBloc() ),
            BlocProvider(create: ( _ ) => MapaBloc() )
          ],
          child: const MaterialApp(
            title: 'CPsP App',
            debugShowCheckedModeBanner: false,
            initialRoute: '/login',
            onGenerateRoute: RouterGenerator.generateRoute,
          ),
        );
      },
    );
  }
}
